# Mechanisms, Organisms and Social Systems

Understanding is explanatory; it is transmitted by answer to _why_ questions.
To understand a system is to be able to explain its properties and behaviour,
and to reveal why it is what it is and wht it behaves the way it does.

information > knowledge > understanding

One can survive without understanding, but not thrive. without understanding
one cannot controle causes; only treat effects, suppress symptoms. With
understanding one can design and create the future.

## The mecahnistic model

It works with a regularity dictated by its internal structure and the causal
laws of nature. The world can be ocmpletely understood and that such
understanding can be obtained by analysis.

Reductionism, understanding of the parts, where the parts also have to be taken
apart. Once the elements are understood, their explanations have to be
aggregated into an understanding of the whole. The relationships between the
parts explain all actions and interactions, cause-effect. 

The exclusive commitment to cause-effect has 3 consequences:
1. The environment is not required to explain anything.
2. Causes themselves require explanation. Treat them as effects and find
   their causes, which must also be explained. There has to be a first cause.
3. All effects are determined by a cause. There's no room for choice or
   purpose.

The way they organize work is a direct consequence of analytical thinking. Work
is reduced to machine-like behaviour and workers are treated like replaceable
machine parts.

Controle and co-ordination are also analysed and reduced to tasks requiring the
minimal amount of power and judgement at each organizational level. Judgement
is further reduced by establishing policies.

Mechanistically modelled organizations are structured hierarchically and are
centrally controlled by an autonomous authority, which can affect any part of
the system. All members of the system, other than the ultimate authority, are
deprived of all information except that required to do their jobs. Even
non-work-related interactions are discouraged.

A mechanistic system is inflexible. It can operate effectively only if its
environment is static or has little effect on it, it operates as a closed
system. Adaption and learning require a readiness, willingness and ability to
change, but these are precisely what mechanistically managed and structured
organizations lack.

## The Organimic model

A social system viewed as an organism has a purpose of its own: suvival for
which growth is taken to be essential. Contraction is associated with
deterioration and decay, with eventual death. It depends on the environment,
which changes, and therefore the system can learn and adapt. With survival in
mind, planning becomes prediction of environmental changes and preparation for
them.

Growth is necessary byt not sufficient for survival. Therefore, the systems try
to reproduce themselves by spawning new organizations or by acquiring old ones.

The executive function is thought of as the brain of the system. It receives
information from sensing organs (diplomatics, intelligence services, marketing
dept) and issue directives to other parts fo the system.

Different parts have some self-control, but they do not controle the functions
they are intended to carry out. Conformity and obedience of the parts is not
essential as long as they perform well. They are managed by control of outputs
rather than inputs.

Organizations try to exercise control by specifyung desired outputs, leaving
the selection of means to the parts (management by objectives). The environment
and organizational performance (outputs) are kept under survaillance to
determine whether they are behaving as expected. If not, corrective action is
taken. Thus, management engages in feedback control. This facilitates both
learning and adaptation.

One's heart cannot decide for itself that it does not want to work or wants to
work for someone else. Differently from an organism, the parts of a social
system have purpose of their own and display choice. An effective social system
requires agreement among its parts and between its parts and the whole. It
requires consensus, an organism does not.


## The social systems model

A system is a whole that cannot be divided into independent parts. The
essential properties of a system are lost when it is taken apart. The parts
themselves lose their essential properties when they are separated from the
whole. A detached steering wheel does not steer and a detached eye does not
see.

Analysis taking a system apart cannot yield _understanding_ of the system, only
_knowledge_. It can reveal a system's structure but not its functions.

The performance of a system is not the sym of the independent performances of
its parts. It's the product of their interactions. Therefore, effective
management of a system requires mamangement of the interactions of its parts,
not their independent actions.

To understand a system, its structure, processes and functions have to be
examined.
* A system's structure is the way its work is divided among its parts and their
  efforts co-ordinated, the relationship between its parts.
* Structure can be understood only if observed in te functioning of the system.
* Therefore, analysis can only reveals the structure of a system, not its
  functioning.

A producer is only necessary, not sufficient, for its product. This makes it
possible to treat choice, hence purpose, as an objectively observable property
of system behaviour. Reference to its environment is required to explain its
product.

Process, rather than initial conditions, is responsible for future states.
Similar conditions (initial states) can lead to dissimilar outcomes (end
states).

Mechanistic conception of a system, speciafic structure causes a particular
function.
```
S1 -> F1
S2 -> F2
S3 -> F3
```

In a social systemic conception of a social system, different structures can
yield the same function, and the same structure can yield different functions.
```
S1 |               | F1
S2 |-> F1 or S1 -> | F2
S3 |               | F3
```

* Reaction: it's sufficient, it is an event that is deterministically caused by
  another event.
* Response: it's necesary but not sufficient. It's an event of which the system
  itself is a coproducer.
* Act: no change in the system's environment is either necessary or sufficient.
  It's self-determined, autonomous behaviour.

Reactive, responsive, and active systems are also correlated with
state-maintaining, goal-seeking and purposeful systems.

A purposeful system is one that can produce the same outcome in different ways
in the same environment and can produce different outcomes in the same and
different environments.

* Mechanistically conceived system is not attributed with a purpose of its own,
  although it serves a purpose of an external controller.
* Organismic systems have a purpose of their own: survival, for which growth is
  necessary.
* A social system has the purpose of self-development.

## Development

A social system, like an individual, can grow by increasing in size or, unlike
an individual, in number without developing. It can also develop without
growing.

Development is not a condition or a state defined by what or how much someone
has. It's more about how much one can do with what one does have. Development
is the process in which individuals increase their abilities and desires to
satisfy their own needs and legitimate desires, and those of others. It is
a least as much a matter of motivation, information, knowledge, understanding,
and wisdom as it is of wealth. Wealth is also relevant because people can
actually improve their quality of life and that of others not only with
motivation, information, and so on, but with instruments and resources
available to them.

A developed man can build a better house with whatever tools and materials he
has than a less developed man with the same resources. A developed man with
limited resources can often improve his quality of life and that of others more
than a less developed man with unlimited resources. The more developed a person
or an organization, the more resources he or it can find or develop.

Social systems cannot develop their members and other stakeholders, but they
can and should encourage and facilitate such development. The development of
such systems consists of an increase in their desire and ability to encourage
and facilitate the development of their stakeholders. Development is not
a matter of what an organization does, but of what it encourages and enables
its members to do.

A limited resource ceases to be  limiting if our need for it decreases or if we
learn how to use it more effectively. That is, if we develop.

Contrains or restrictions on a social system's growth are found primarily in
its environment; but the principal limits to constraints on its development are
found within the system itself. The key to unlimited development is freedom of
choice that is limited only to those who do not limit the couce of others. That
is, freedom that is exercised morally.

## Conclusion

Social-system management is concerned with development and tries to server the
purpose of the system, its parts, and its containing systems. Resolution or
dissolution of conflict is one of management's principal responsibilities. For
managements of social systems, planning should consist of designing a desirable
future and inventing or finding ways of approximating it as closely as
possible. Such management should attempt tp maximize the freedom of choice of
those ir affects. Only from experience of choice can one learn, hence develop. 

