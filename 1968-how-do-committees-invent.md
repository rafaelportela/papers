# How do committees invent?

Melvin E. Conway, 1968

http://www.melconway.com/Home/Committees_Paper.html

The design organization may or may not be involved in the construction of the
system it designs. [...] It seems reasonable to suppose that the knowledge that
one will have to carry out one's own recommendations or that this task will
fall to others, probably affects some design choices which the individual
designer is called upon to make.

We are now in a position to address the fundamental question of this article.
Is there any predictable relationship between the graph structure of a design
organization and the graph structure of the system it designs? The answer is:
Yes, the relationship is so simple that in some cases it is an identity.

In the not unusual case where each subsystem had its own separate design group,
we find that the structures of the design group and the system are identical.
In the case where some group designed more than one subsystem we find that the
structure of the design organization is a collapsed version of the structure of
the system, with the subsystems having the same design group collapsing into
one group. This kind of a structure-preserving relationship between two sets of
things is called a homomorphism.

*Systems Image Their Design Groups*

A contract research organization had eight people who were to produce a COBOL
and an ALGOL compiler. [...] Five people were assigned to the COBOL job and
three to the ALGOL job. The resulting COBOL compiler ran in five phases, the
ALGOL compiler ran in three.

*System Management*

Assuming that two men and one hundred men cannot work in the same
organizational structure our homomorphism says that they will not design
similar systems; therefore the value of their efforts may not even be
comparable. From experience we know that the two men, if they are well chosen
and survive the experience, will give us a better system. Assumptions which may
be adequate for peeling potatoes and erecting brick walls fail for designing
systems.

As long as the manager's prestige and power are tied to the size of his budget,
he will be motivated to expand his organization. This is an inappropriate
motive in the management of a system design activity.

Even in a moderately small organization it becomes necessary to restrict
communication in order that people can get some "work" done. Research which
leads to techniques permitting more efficient communication among designers
will play an extremely important role in the technology of system management.

If, then, it is reasonable to assume that for any system requirement there is
a family of system designs which will meet that requirement, we must also
inquire whether the choice of design organization influences the process of
selection of a system design from that family. If we believe our homomorphism,
then we must agree that it does.

Once the organization exists, of course, it will be used. Probably the greatest
single common factor behind many poorly designed systems now in existence has
been the availability of a design organization in need of work.

Organizations which design systems (in the broad sense used here) are
constrained to produce designs which are copies of the communication structures
of these organizations. Primarily, we have found a criterion for the
structuring of design organizations: a design effort should be organized
according to the need for communication.

